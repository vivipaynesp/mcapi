<?php

namespace Script;

use Script\Entity\Account;
use Script\Entity\Card;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Gedmo\Loggable\LoggableListener;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\Dotenv\Dotenv;
use RuntimeException;

$loader = require __DIR__ . '/../vendor/autoload.php';
AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

// Load cached env vars if the .env.local.php file exists
// Run "composer dump-env prod" to create it (requires symfony/flex >=1.2)
if (is_array($env = @include dirname(__DIR__) . '/.env.local.php')) {
    foreach ($env as $k => $v) {
        $_ENV[$k] = $_ENV[$k] ?? (isset($_SERVER[$k]) && 0 !== strpos($k, 'HTTP_') ? $_SERVER[$k] : $v);
    }
} elseif (!class_exists(Dotenv::class)) {
    throw new RuntimeException('Please run "composer require symfony/dotenv" to load the ".env" files configuring the application.');
} else {
    // load all the .env files
    (new Dotenv(false))->loadEnv(dirname(__DIR__) . '/.env');
}

$_SERVER += $_ENV;
$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = ($_SERVER['APP_ENV'] ?? $_ENV['APP_ENV'] ?? null) ?: 'dev';

// database connection
$config = Setup::createAnnotationMetadataConfiguration(
    array(__DIR__ . 'Entity'), false, null, null, false
);
$namingStrategy = new UnderscoreNamingStrategy(CASE_LOWER);
$config->setNamingStrategy($namingStrategy);

if ($_SERVER['APP_ENV'] == 'dev') {
    $conn = array(
        'driver' => 'mysql',
        'url' => 'mysql://dummy:uKacNYFqFqbjU0aD8Vw0rvEF1V27B6wM@127.0.0.1:3306/mcapi',
    );
} else {
    $conn = array(
        'driver' => 'mysql',
        'url' => 'mysql://admin:masterkey@rede-facil.ccmq4atipdxi.us-east-1.rds.amazonaws.com:3306/mcapi',
    );
}

$entityManager = EntityManager::create($conn, $config);
$LoggableListener = new LoggableListener();
$LoggableListener->setUsername('admin');
$entityManager->getEventManager()->addEventSubscriber($LoggableListener);
// https://s3.amazonaws.com/wallet-v2/marita-reports/MARITA20191124.zip
$str = file_get_contents(__DIR__ . '/../src/DataFixtures/json/MARITA20191124.json');
$json = json_decode($str, false);
$date = new \DateTime();

foreach ($json as $item) {
    /** @var Account|null $account */
    $account = $entityManager
        ->getRepository(Account::class)
        ->findOneBy(['cpf' => $item->cpf]);

    if (is_null($account)) {
        $account = new Account();
        $account->setCpf($item->cpf);

        foreach ($item->cards as $subitem) {
            $card = new Card();
            $card->setProxy((string)$subitem->proxy);
            $card->setActive($subitem->active);
            $account->addCard($card);
        }
    } else {
        foreach ($item->cards as $subitem) {
            /** @var Card|null $card */
            $card = $entityManager
                ->getRepository(Card::class)
                ->findOneBy(['account' => $account->getId(), 'proxy' => (string)$subitem->proxy]);

//            print_r('Account: ' . $account->getId() . ' Card: ' . $card->getProxy() . PHP_EOL);

            if (is_null($card)) {
                $card = new Card();
                $card->setProxy((string)$subitem->proxy);
                $card->setActive($subitem->active);
                $account->addCard($card);
            } else {
                if ($card->getActive() != $subitem->active) {
                    $card->setActive($subitem->active);
                }
            }
        }
    }

    $entityManager->persist($account);
}

$entityManager->flush();

// clean up database for deleted accounts
/** @var Account[] $accounts */
$accounts = $entityManager
    ->getRepository(Account::class)
    ->findAll();

foreach ($accounts as $account) {
    $exists = false;
    foreach ($json as $item) {
        if ($item->cpf == $account->getCpf()) {
            $exists = true;
        }
    }

    if (!$exists) {
        print_r('The following account was deleted: ' . $account->getCpf() . ' but was kept in our system.' . PHP_EOL);

        foreach ($account->getCards() as $card) {
            $card->setActive(false);
            print_r('The following cards were deleted: ' . $card->getProxy() . ' but was set inactive in our system.' . PHP_EOL);
        }
    }

    $entityManager->persist($account);
}

$entityManager->flush();
