<?php

namespace Script\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Account.
 *
 * @ORM\Entity()
 * @ORM\Table(name="accounts",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="cpf_unique", columns={"cpf"})},
 *     indexes={@ORM\Index(name="cpf_idx", columns={"cpf"})}
 * )
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"get_account"}, "enable_max_depth"=true},
 *     denormalizationContext={"groups"={"create_acount"}, "enable_max_depth"=true},
 *     attributes={"validation_groups"={"create_acount", "get_account"}}
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"cpf": "partial"})
 * @ApiFilter(DateFilter::class, properties={"updatedAt"})
 * @ApiFilter(BooleanFilter::class, properties={"cards.active"})
 *
 * @Gedmo\Loggable
 *
 * @package Script\Entity
 *
 * @author Jesse Quinn
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Assert\NotNull()
     *
     * @Gedmo\Versioned
     */
    private $cpf;

    /**
     * @ORM\OneToMany(targetEntity="Script\Entity\Card", mappedBy="account", cascade={"persist"}, orphanRemoval=true)
     *
     * @Groups({"create_acount", "get_account"})
     */
    private $cards;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Gedmo\Versioned
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Gedmo\Versioned
     */
    private $updatedAt;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setAccount($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            // set the owning side to null (unless already changed)
            if ($card->getAccount() === $this) {
                $card->setAccount(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
