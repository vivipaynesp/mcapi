<?php

namespace Script\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cards",
 *     indexes={@ORM\Index(name="proxy_idx", columns={"proxy"})}
 * )
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"get_account"}, "enable_max_depth"=true},
 *     denormalizationContext={"groups"={"create_acount"}, "enable_max_depth"=true},
 *     attributes={"validation_groups"={"create_acount", "get_account"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"proxy": "partial"})
 * @ApiFilter(DateFilter::class, properties={"updatedAt"})
 *
 * @Gedmo\Loggable
 *
 * @package Script\Entity
 *
 * @author Jesse Quinn
 */
class Card
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * ReactJS limitation with Bigint.
     * Use string instead.
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Gedmo\Versioned
     */
    private $proxy;

    /**
     * @ORM\Column(name="active", type="boolean")
     *
     * @Assert\NotNull()
     * @Assert\Choice({"true", "false"})
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Gedmo\Versioned
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="Script\Entity\Account", inversedBy="cards", cascade={"persist"})
     * @ORM\JoinColumn(name="account", referencedColumnName="id")
     *
     * @Assert\NotNull()
     *
     * @Gedmo\Versioned
     */
    private $account;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Gedmo\Versioned
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version
     *
     * @Groups({"create_acount", "get_account"})
     *
     * @Gedmo\Versioned
     */
    private $updatedAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProxy(): ?string
    {
        return $this->proxy;
    }

    public function setProxy(string $proxy): self
    {
        $this->proxy = $proxy;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
