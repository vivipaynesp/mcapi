<?php

namespace App\Tests\Tool;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class BaseTestCaseORM.
 *
 * @package App\Tests\Tool
 *
 * @author Jesse Quinn
 */
abstract class BaseTestCaseORM extends KernelTestCase
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     *
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
