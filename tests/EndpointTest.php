<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class EndpointTest.
 *
 * @package App\Tests
 * @author Jesse Quinn
 */
class EndpointTest extends ApiTestCase
{
    /** @var string $token */
    private $token;

    /**
     * Test login endpoint and setup bearer for all connections.
     */
    public function setup(): void
    {
        $response = static::createClient()->request(
            'POST',
            '/authenticate',
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => [
                    'email' => 'test',
                    'password' => 'test'
                ]
            ]);
        $this->assertResponseIsSuccessful();
        $json = json_decode($response->getContent());
        $this->token = $json->token;
        $this->assertIsString($this->token);
    }

    /**
     * Test accounts endpoint.
     *
     * @throws TransportExceptionInterface
     */
    public function testAccountCollection(): void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/accounts',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token
                ]
            ]
        );
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseStatusCodeSame(200, $response->getStatusCode());
    }
}
