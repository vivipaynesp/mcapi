<?php

namespace App\Tests;

use App\Entity\Account;
use App\Entity\Card;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Gedmo\Loggable\LoggableListener;
use App\Tests\Tool\BaseTestCaseORM;

/**
 * Class PopulateDatabaseTest.
 *
 * @package App\Tests
 *
 * @author Jesse Quinn
 */
class PopulateDatabaseTest extends BaseTestCaseORM
{
    /** @var LoggableListener */
    private $LoggableListener;

    protected function setUp()
    {
        parent::setUp();

        $this->LoggableListener = new LoggableListener();
        $this->LoggableListener->setUsername('admin');
        $this->entityManager->getEventManager()->addEventSubscriber($this->LoggableListener);
    }

//    /**
//     * Read json file.
//     */
//    public function testReadJson()
//    {
//        $str = file_get_contents(__DIR__ . '/../src/DataFixtures/json/example.json');
//        $json = json_decode($str, false);
//
//        foreach ($json as $account) {
//            $this->assertInternalType("string", $account->cpf);
//
//            foreach ($account->cards as $card) {
//                $this->assertInternalType('int', $card->active);
//            }
//        }
//
//        $this->assertJson($str);
//    }

//    /**
//     * @throws ORMException
//     * @throws OptimisticLockException
//     */
//    public function testPopulateDatabase()
//    {
//        $str = file_get_contents(__DIR__ . '/../src/DataFixtures/json/example.json');
//        $json = json_decode($str, false);
//        $date = new \DateTime();
//
//        foreach ($json as $item) {
//            $account = new Account();
//            $account->setCpf($item->cpf);
//            $this->assertInternalType("string", $item->cpf);
//
//            foreach ($item->cards as $subitem) {
//                $card = new Card();
//                $card->setProxy((string)$subitem->proxy);
//                $card->setActive(($subitem->active == 1) ? true : false);
//                $account->addCard($card);
//                $this->assertInternalType('int', $subitem->active);
//            }
//
//            $this->entityManager->persist($account);
//        }
//
//        $this->entityManager->flush();
//    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testPopulateDatabaseUpdate()
    {
        $str = file_get_contents(__DIR__ . '/../src/DataFixtures/json/MARITA20191106.json');
        $json = json_decode($str, false);
        $date = new \DateTime();

        foreach ($json as $item) {
            $account = $this->entityManager
                ->getRepository(Account::class)
                ->findOneBy(['cpf' => $item->cpf]);

            if (is_null($account)) {
                $account = new Account();
                $account->setCpf($item->cpf);
                $this->assertInternalType("string", $item->cpf);

                foreach ($item->cards as $subitem) {
                    $card = new Card();
                    $card->setProxy((string)$subitem->proxy);
                    $card->setActive(($subitem->active == 1) ? true : false);
                    $account->addCard($card);
                    $this->assertInternalType('int', $subitem->active);
                }
            } else {
                foreach ($item->cards as $subitem) {
                    $card = $this->entityManager
                        ->getRepository(Card::class)
                        ->findOneBy(['proxy' => (string)$subitem->proxy]);

                    if (is_null($card)) {
                        $card = new Card();
                        $card->setProxy((string)$subitem->proxy);
                        $card->setActive(($subitem->active == 1) ? true : false);
                        /** @var Account $account */
                        $account->addCard($card);
                        $this->assertInternalType('int', $subitem->active);
                    } else {
                        /** @var Card $card */
                        if ($card->getActive() != $subitem->active) {
                            $card->setActive(($subitem->active == 1) ? true : false);
                            $this->assertInternalType('int', $subitem->active);
                        }
                    }
                }
            }

            $this->entityManager->persist($account);
            $this->entityManager->flush();
        }

    }

    protected function tearDown()
    {
        parent::tearDown();

    }
}