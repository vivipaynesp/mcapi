<?php

namespace App\Exception;

use Exception;

/**
 * Class DuplicateUserException.
 *
 * @package App\Exception
 *
 * @author Jesse Quinn
 */
final class DuplicateUserException extends Exception
{

}