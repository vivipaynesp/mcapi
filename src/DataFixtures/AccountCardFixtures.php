<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Card;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Gedmo\Loggable\LoggableListener;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AccountCardFixtures.
 *
 * @package App\DataFixtures
 *
 * @author Jesse Quinn
 */
class AccountCardFixtures extends Fixture
{
    /** @var LoggableListener */
    private $loggableListener;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * AccountCardFixtures constructor.
     *
     * @param LoggableListener $loggableListener
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(LoggableListener $loggableListener, EntityManagerInterface $entityManager)
    {
        $this->loggableListener = $loggableListener;
        $this->loggableListener->setUsername('admin');
        $this->entityManager = $entityManager;
        $this->entityManager->getEventManager()->addEventSubscriber($loggableListener);
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $str = file_get_contents(__DIR__ . '/../DataFixtures/json/MARITA20191106.json');
        $json = json_decode($str, false);
        $date = new \DateTime();
//        $date->modify('-2 month'); // testing purposes only

        foreach ($json as $item) {
            $account = new Account();
            $account->setCpf($item->cpf);

            foreach ($item->cards as $subitem) {
                $card = new Card();
                $card->setProxy((string)$subitem->proxy);
                $card->setActive(($subitem->active == 1) ? true : false);
                $account->addCard($card);
            }
            $manager->persist($account);
        }

        $manager->flush();
    }
}
