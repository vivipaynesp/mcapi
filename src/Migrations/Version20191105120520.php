<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191105120520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_1483A5E9F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__users AS SELECT id, password FROM users');
        $this->addSql('DROP TABLE users');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        )');
        $this->addSql('INSERT INTO users (id, password) SELECT id, password FROM __temp__users');
        $this->addSql('DROP TABLE __temp__users');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('DROP INDEX proxy_unique');
        $this->addSql('DROP INDEX IDX_4C258FD7D3656A4');
        $this->addSql('CREATE TEMPORARY TABLE __temp__cards AS SELECT id, account, proxy, active FROM cards');
        $this->addSql('DROP TABLE cards');
        $this->addSql('CREATE TABLE cards (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, account INTEGER DEFAULT NULL, proxy VARCHAR(255) NOT NULL COLLATE BINARY, active BOOLEAN NOT NULL, CONSTRAINT FK_4C258FD7D3656A4 FOREIGN KEY (account) REFERENCES accounts (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO cards (id, account, proxy, active) SELECT id, account, proxy, active FROM __temp__cards');
        $this->addSql('DROP TABLE __temp__cards');
        $this->addSql('CREATE UNIQUE INDEX proxy_unique ON cards (proxy)');
        $this->addSql('CREATE INDEX IDX_4C258FD7D3656A4 ON cards (account)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_4C258FD7D3656A4');
        $this->addSql('DROP INDEX proxy_unique');
        $this->addSql('CREATE TEMPORARY TABLE __temp__cards AS SELECT id, account, proxy, active FROM cards');
        $this->addSql('DROP TABLE cards');
        $this->addSql('CREATE TABLE cards (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, account INTEGER DEFAULT NULL, proxy VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO cards (id, account, proxy, active) SELECT id, account, proxy, active FROM __temp__cards');
        $this->addSql('DROP TABLE __temp__cards');
        $this->addSql('CREATE INDEX IDX_4C258FD7D3656A4 ON cards (account)');
        $this->addSql('CREATE UNIQUE INDEX proxy_unique ON cards (proxy)');
        $this->addSql('DROP INDEX UNIQ_1483A5E9E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__users AS SELECT id, password FROM users');
        $this->addSql('DROP TABLE users');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, password VARCHAR(500) NOT NULL COLLATE BINARY, username VARCHAR(25) NOT NULL COLLATE BINARY, is_active BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO users (id, password) SELECT id, password FROM __temp__users');
        $this->addSql('DROP TABLE __temp__users');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
    }
}
