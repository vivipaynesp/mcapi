<?php

namespace App\EventSubscriber;

use Gedmo\Loggable\LoggableListener;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DoctrineExtensionSubscriber.
 *
 * @package App\EventSubscriber
 *
 * @author  Jesse Quinn
 */
class DoctrineExtensionSubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var LoggableListener
     */
    private $loggableListener;

    /**
     * DoctrineExtensionSubscriber constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param LoggableListener $loggableListener
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        LoggableListener $loggableListener
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->loggableListener = $loggableListener;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest'
        ];
    }

    /**
     *
     */
    public function onKernelRequest(): void
    {
        if ($this->tokenStorage !== null &&
            $this->tokenStorage->getToken() !== null &&
            $this->tokenStorage->getToken()->isAuthenticated() === true
        ) {
            $this->loggableListener->setUsername($this->tokenStorage->getToken()->getUser());
        }
    }
}
