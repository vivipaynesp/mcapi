<?php

namespace App\EventSubscriber;

use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionToJsonResponseSubscriber.
 *
 * Creates custom error message for any /api endpoint that does not exist.
 *
 * @package App\EventSubscriber
 *
 * @author Jesse Quinn
 */
final class ExceptionToJsonResponseSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        // Skip if request is not an API-request
        $request = $event->getRequest();

//        if (strpos($request->getPathInfo(), '/api') !== 0) {
//            return;
//        }

        $exception = $event->getException();
        $error = [
            'errors' => [
                'status' => (string)$this->getStatusCodeFromException($exception),
                'source' => [
                    'pointer' => $request->getPathInfo()
                ],
                'title' => $this->getErrorTypeFromException($exception),
                'detail' => $exception->getMessage()
            ]
        ];
        $response = new JsonResponse($error, $this->getStatusCodeFromException($exception));
        $event->setResponse($response);
    }

    /**
     * @param Exception $exception
     *
     * @return int
     */
    private function getStatusCodeFromException(Exception $exception): int
    {
        if ($exception instanceof HttpException) {
            return $exception->getStatusCode();
        }

        return 500;
    }

    /**
     * @param Exception $exception
     *
     * @return string
     */
    private function getErrorTypeFromException(Exception $exception): string
    {
        $parts = explode('\\', get_class($exception));

        return end($parts);
    }
}
