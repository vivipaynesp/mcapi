[![Build Status](https://drone.vivitech-bnu.com.br/api/badges/vivipaynesp/mcapi/status.svg)](https://drone.vivitech-bnu.com.br/vivipaynesp/mcapi)

### MaritaCASH API
Ignite API via:

       $ make docker-start    

Makefile will install composer dependencies and yarn dependencies.

Establish database:
    
    $ make doctrine-update

JWT Keys - needed for API bearer encryption/user creation/etc.

```bash
mkdir -p config/jwt
jwt_passhrase=$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')
echo "$jwt_passhrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
echo "$jwt_passhrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
```

Test user registration and login:

    $ make api-dummy
    $ make api-token

Refer to [Postman](https://documenter.getpostman.com/view/9113626/SW15xGLw?version=latest) documentation for available endpoints
or go to `http://localhost/admin#/login` to interact with `api-platform admin`.

*GraphQl supported*

#### Minor Details - Development
It is assumed that `php 7.3.10` is installed other update
`.php-version` with current php version installed:

    $ symfony local:php:list

will produce a list of available php versions.

To create API entities:

    $ bin/console make:entity --api-resource Entity
      
An example response from `/api/accounts`:

```json
{
  "@context": "/api/contexts/Account",
  "@id": "/api/accounts",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "/api/accounts/1",
      "@type": "Account",
      "cpf": "01293576344",
      "cards": [
        {
          "@id": "/api/cards/1",
          "@type": "Card",
          "proxy": 6371691901203394000,
          "active": true
        },
        {
          "@id": "/api/cards/2",
          "@type": "Card",
          "proxy": 6371691901203394000,
          "active": true
        },
        {
          "@id": "/api/cards/3",
          "@type": "Card",
          "proxy": 6371691901203394000,
          "active": false
        },
        {
          "@id": "/api/cards/4",
          "@type": "Card",
          "proxy": 6371691901203394000,
          "active": false
        }
      ]
    },
    {
      "@id": "/api/accounts/2",
      "@type": "Account",
      "cpf": "14302856726",
      "cards": [
        {
          "@id": "/api/cards/5",
          "@type": "Card",
          "proxy": 6371691901203395000,
          "active": true
        },
        {
          "@id": "/api/cards/6",
          "@type": "Card",
          "proxy": 6371691901204071000,
          "active": false
        }
      ]
    },
    {
      "@id": "/api/accounts/3",
      "@type": "Account",
      "cpf": "04747509739",
      "cards": [
        {
          "@id": "/api/cards/7",
          "@type": "Card",
          "proxy": 6371691901203748000,
          "active": true
        },
        {
          "@id": "/api/cards/8",
          "@type": "Card",
          "proxy": 6371691901204071000,
          "active": false
        },
        {
          "@id": "/api/cards/9",
          "@type": "Card",
          "proxy": 6371691901204072000,
          "active": false
        }
      ]
    },
    {
      "@id": "/api/accounts/4",
      "@type": "Account",
      "cpf": "04541158906",
      "cards": [
        {
          "@id": "/api/cards/10",
          "@type": "Card",
          "proxy": 6371691901202930000,
          "active": false
        },
        {
          "@id": "/api/cards/11",
          "@type": "Card",
          "proxy": 6371691901203722000,
          "active": false
        },
        {
          "@id": "/api/cards/12",
          "@type": "Card",
          "proxy": 6371691901203634000,
          "active": false
        },
        {
          "@id": "/api/cards/13",
          "@type": "Card",
          "proxy": 6371691901203606000,
          "active": true
        }
      ]
    }
  ],
  "hydra:totalItems": 4
}
```

Load datafixture:

    $ bin/console doctrine:fixtures:load
    
View mapping of classes:

    $ bin/console doctrine:mapping:info
    
     [OK]   App\Entity\Users
     [OK]   App\Entity\Account
     [OK]   App\Entity\Card
     [OK]   Gedmo\Loggable\Entity\MappedSuperclass\AbstractLogEntry
     [OK]   Gedmo\Loggable\Entity\LogEntry
