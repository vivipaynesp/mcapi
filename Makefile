help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  clean                                  Clean directories for reset"
	@echo "  composer-up                            Update PHP dependencies with composer"
	@echo "  yarn-install                           Install NPM dependencies with yarn"
	@echo "  api-register                           Create test API account"
	@echo "  api-token                              Get dummy account token"
	@echo "  docker-start                           Create and start containers"
	@echo "  docker-stop                            Stop and clear all services"
	@echo "  doctrine-create                        Create database"
	@echo "  doctrine-update                        Update database"
	@echo "  doctrine-create-migration              Create migrations"
	@echo "  doctrine-migrate                       Execute a migration to a specified version or the latest available version"
	@echo "  doctrine-load-fixtures                 Loads fixtures"
	@echo "  doctrine-schema-drop                   Dump all tables"
	@echo "  doctrine-make-diff                     Generate a migration by comparing your current database to your mapping information"
	@echo "  doctrine-make-diff-up                  Execute a single migration version up"
	@echo "  doctrine-make-entity                   Create entity"
	@echo "  regenerate                             Regenerate getters, setters and repository classes for entities"
	@echo "  warm-cache                             Warm up cache"
	@echo "  assets-install                         Install assets"

init:
	@if [ "$(APP_ENV)" = "prod" ]; then \
		cp -n $(shell pwd)/.env.prod $(shell pwd)/.env 2> /dev/null; \
		cp -n $(shell pwd)/docker-compose.yml.prod $(shell pwd)/docker-compose.yml 2> /dev/null; \
	else \
		cp -n $(shell pwd)/.env.dist $(shell pwd)/.env 2> /dev/null; \
		cp -n $(shell pwd)/docker-compose.yml.dist $(shell pwd)/docker-compose.yml 2> /dev/null; \
	fi

clean:
	@rm -Rf vendor
	@rm -Rf var
	@rm -Rf node_modules
	@rm -Rf package-lock.json
	@rm -Rf .env
	@rm -Rf docker-compose.yml

composer-up:
	@docker run --rm -v $(shell pwd)/:/app composer update

yarn-install:
	@docker run --rm -v $(shell pwd)/:/app -w /app node:latest yarn install
	@docker run --rm -v $(shell pwd)/:/app -w /app node:latest yarn run build

api-register:
	@curl -X POST -H "Content-Type: application/json" "http://localhost/register?email=test&password=test"

api-token:
	@curl -X POST -H "Content-Type: application/json" http://localhost/authenticate -d '{"email":"test","password":"test"}'

doctrine-create:
	@docker-compose exec -T php-fpm php bin/console doctrine:database:create

doctrine-update:
	@docker-compose exec -T php-fpm php bin/console doctrine:schema:update -f

doctrine-create-migration:
	@docker-compose exec -T php-fpm php bin/console make:migration

doctrine-migrate:
	@docker-compose exec -T php-fpm php bin/console doctrine:migrations:migrate

doctrine-load-fixtures:
	@docker-compose exec -T php-fpm php bin/console doctrine:fixtures:load

doctrine-schema-drop:
	@docker-compose exec -T php-fpm php bin/console doctrine:schema:drop --full-database --force

doctrine-make-entity:
	@docker-compose exec php-fpm php bin/console make:entity

doctrine-make-diff:
	@docker-compose exec -T php-fpm php bin/console doctrine:migrations:diff

doctrine-make-diff-up:
	@docker-compose exec -T php-fpm php bin/console doctrine:migrations:execute --up $(migration)

regenerate:
	@docker-compose exec php-fpm php bin/console make:entity --regenerate

warm-cache:
	@docker-compose exec php-fpm php bin/console cache:clear --env=${APP_ENV}

assets-install:
	@docker-compose exec php-fpm php bin/console assets:install

php-test:
	@docker-compose exec -T php-fpm php bin/phpunit

docker-start: clean init composer-up yarn-install resetOwner
	docker-compose up -d
	@make assets-install
	@make warm-cache

docker-stop: resetOwner
	@docker-compose stop
	@make clean

resetOwner:
	@$(shell sudo chown -Rf $(shell id -u):$(shell id -g) "$(shell pwd)/" 2> /dev/null)

.PHONY: clean init